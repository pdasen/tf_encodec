import math
import pickle

import numpy as np
import torch

#from audiotools.data.datasets import AudioDataset, AudioLoader
from tifresi.metrics import projection_loss
from illm.train_audio import build_module, pretrained_state_dict
from audiotools import AudioSignal

# Image metrics
from torchmetrics.image import (
    #FrechetInceptionDistance,
    PeakSignalNoiseRatio,
    MultiScaleStructuralSimilarityIndexMeasure
)
# Audio metrics
from torchmetrics.audio import (
    ScaleInvariantSignalDistortionRatio,
)
import audiotools.metrics as audiomt
from frechet_audio_distance import FrechetAudioDistance

from encodec import EncodecModel
import dac
import julius

from tf_encodec.conversion import TFConverter
from tf_encodec.utils import suppress_noise

device = torch.device("cpu")

class ImageMetrics:
    def __init__(self):
        self.psnr = PeakSignalNoiseRatio()
        self.msssim = MultiScaleStructuralSimilarityIndexMeasure()

    def get_metrics(self, original, decompressed):
        return {"psnr" : self.psnr(original, decompressed),
                "ms-ssim" : self.msssim(original, decompressed)}

    def summary(self):
        return {"psnr" : self.psnr.compute(),
                "ms-ssim" : self.msssim.compute()}


class AudioMetrics:
    def __init__(self, enable_fad=True):
        self.sisdr = ScaleInvariantSignalDistortionRatio()
        self.visqol_values = np.array([])
        self.mel_distance_calc = audiomt.spectral.MelSpectrogramLoss()
        self.mel_distance_values = np.array([])

        # Turn off warnings and stdout for FrechetAudioDistance()
        self.enable_fad = enable_fad
        if enable_fad:
            with suppress_noise():
                self.frechet = FrechetAudioDistance(
                    model_name="clap",
                    sample_rate=48000,
                    submodel_name="630k-audioset",  # for CLAP only
                    verbose=False,
                    enable_fusion=False,            # for CLAP only
                )
            self.frechet_embds_original = []
            self.frechet_embds_reconstructed = []

    def get_metrics(self, original, reconstructed):
        #sample_rate = self.cfg["audio"]["sample_rate"]
        #original = AudioSignal(original, sample_rate=sample_rate)
        #reconstructed = AudioSignal(reconstructed, sample_rate=sample_rate)

        mel_distance = self.mel_distance_calc(reconstructed, original)
        visqol_similarity = audiomt.quality.visqol(reconstructed, original)
        #visqol_similarity = 1.0 # BUG: visqol is not working on cluster

        self.mel_distance_values = np.append(self.mel_distance_values, mel_distance)
        self.visqol_values = np.append(self.visqol_values, visqol_similarity)

        if self.enable_fad:
            # Resample to 48kHz for FAD
            orig48 = original.clone().to_mono().resample(48000)
            rec48 = reconstructed.clone().to_mono().resample(48000)
            with torch.no_grad():
                embd_orig = self.frechet.model.get_audio_embedding_from_data(
                    orig48.audio_data[0], use_tensor=True)
                embd_rec = self.frechet.model.get_audio_embedding_from_data(
                    rec48.audio_data[0], use_tensor=True)
            self.frechet_embds_original.append(embd_orig.squeeze().numpy())
            self.frechet_embds_reconstructed.append(embd_rec.squeeze().numpy())

        return {"sisdr" : self.sisdr(original.audio_data, reconstructed.audio_data),
                "mel_distance" : mel_distance,
                "visqol" : visqol_similarity}

    def summary(self):
        if self.enable_fad:
            mu_orig, sigma_orig = self.frechet.calculate_embd_statistics(self.frechet_embds_original)
            mu_rec, sigma_rec = self.frechet.calculate_embd_statistics(self.frechet_embds_reconstructed)
            fad_score = self.frechet.calculate_frechet_distance(mu_orig, sigma_orig, mu_rec, sigma_rec)
        else:
            fad_score = math.nan

        return {"sisdr" : self.sisdr.compute(),
                "mel_distance" : np.mean(self.mel_distance_values),
                "visqol" : np.mean(self.visqol_values),
                "fad" : fad_score}


class CompressionModelAudio:
    def __init__(self, sample_rate, audio_metrics):
        self.sample_rate = sample_rate
        # For now we only support 44.1kHz
        self.sample_rate = 44100
        self.audio_metrics = audio_metrics

    def compress(self, audio_data):
        return audio_data

    def decompress(self, compressed_data):
        return compressed_data

    def get_bitrate(self, compressed_data):
        return {"bitrate": 0.0}

    def evaluate_batch(self, batch):
        self.batch_sig = batch["signal"].normalize()
        self.compressed = self.compress(self.batch_sig.audio_data)
        self.decompressed = AudioSignal(self.decompress(self.compressed), self.sample_rate).normalize()

        metrics = self.audio_metrics.get_metrics(self.batch_sig, self.decompressed)
        bitrate = self.get_bitrate(self.compressed)
        return metrics | bitrate

    def get_summary(self):
        return self.audio_metrics.summary()

    def get_artifacts(self):
        return {"original": self.batch_sig.audio_data[0,:].numpy().squeeze(),
                "decompressed": self.decompressed[0,:].numpy().squeeze()}


class CompressionModelDescriptCodec(CompressionModelAudio):
    def __init__(self, sample_rate, audio_metrics, target_bandwidth=8.0):
        super().__init__(sample_rate, audio_metrics)
        # Download a model
        self.model_path = dac.utils.download(model_type="44khz")
        self.model = dac.DAC.load(self.model_path)

    def compress(self, audio_data):
        signal = AudioSignal(audio_data, sample_rate=self.sample_rate)
        return self.model.compress(signal)

    def decompress(self, compressed_data):
        return self.model.decompress(compressed_data).audio_data

    def get_bitrate(self, compressed_data):
        return {"bitrate": 16.0}


class CompressionModelEncodec(CompressionModelAudio):
    def __init__(self, sample_rate, audio_metrics, model_input_sample_rate, target_bandwidth=12.0):
        super().__init__(sample_rate, audio_metrics)
        self.model_input_sample_rate = model_input_sample_rate
        match model_input_sample_rate:
            case 48000:
                self.model = EncodecModel.encodec_model_48khz()
            case 24000:
                self.model = EncodecModel.encodec_model_24khz()
            case default:
                raise ValueError("Unsupported model input sample rate")

        # The number of codebooks used will be determined bythe bandwidth selected.
        # E.g. for a bandwidth of 6kbps, `n_q = 8` codebooks are used.
        # Supported bandwidths are 1.5kbps (n_q = 2), 3 kbps (n_q = 4), 6 kbps (n_q = 8) and 12 kbps (n_q =16) and 24kbps (n_q=32).
        # For the 48 kHz model, only 3, 6, 12, and 24 kbps are supported. The number
        # of codebooks for each is half that of the 24 kHz model as the frame rate is twice as much.
        self.bitrate = target_bandwidth
        self.model.set_target_bandwidth(target_bandwidth)

    def compress(self, audio_data):
        return self.model.encode(audio_data)

    def decompress(self, compressed_data):
        return self.model.decode(compressed_data)

    def evaluate_batch(self, batch):
        self.batch_sig = batch["signal"].normalize()
        preprocessed = self.batch_sig.clone()
        # Resample
        preprocessed = preprocessed.resample(self.model_input_sample_rate).audio_data
        # Expand to stereo if needed
        if self.model_input_sample_rate == 48000:
            preprocessed = preprocessed.expand(-1, 2, -1)

        with torch.no_grad():
            self.compressed = self.compress(preprocessed)
            temp_decompressed = self.decompress(self.compressed)

        if self.model_input_sample_rate == 48000:
            # Downmix to mono
            temp_decompressed = temp_decompressed.mean(-2, keepdim=True)

        resampled_mono = julius.resample_frac(temp_decompressed, self.model_input_sample_rate, 44100)
        # Trim whichever audio signal is longer
        if resampled_mono.shape[-1] > self.batch_sig.audio_data.shape[-1]:
            resampled_mono = resampled_mono[...,:self.batch_sig.audio_data.shape[-1]]
        else:
            self.batch_sig.audio_data = self.batch_sig.audio_data[...,:resampled_mono.shape[-1]]

        self.decompressed = AudioSignal(resampled_mono, 44100).normalize()

        metrics = self.audio_metrics.get_metrics(self.batch_sig, self.decompressed)
        bitrate = self.get_bitrate(self.compressed)
        return metrics | bitrate

    def get_bitrate(self, compressed_data):
        return {"bitrate": self.bitrate}


class CompressionModelSpectrogram(CompressionModelAudio):
    def __init__(self, sample_rate, audio_metrics, image_metrics, converter: TFConverter):
        super().__init__(sample_rate, audio_metrics)
        self.image_metrics = image_metrics
        self.converter = converter

    # Compress and decompress aren't needed in evaluation script but maybe elsewhere
    def compress(self, audio_data):
        spectrograms = self.converter.convert_batch_to_spectrograms(audio_data)
        return self.compress_spectrograms(spectrograms)

    def decompress(self, compressed_data):
        decompressed = self.decompress_spectrograms(compressed_data)
        return self.converter.invert_batch_spectrograms(decompressed)

    # No-ops
    def compress_spectrograms(self, spectrograms):
        return spectrograms

    def decompress_spectrograms(self, compressed_data):
        return compressed_data

    def evaluate_batch(self, batch):
        self.batch_sig = batch["signal"].normalize()
        self.spectrograms = self.converter.convert_batch_to_spectrograms(self.batch_sig.audio_data)
        self.compressed_spectrograms = self.compress_spectrograms(self.spectrograms)
        self.decompressed_spectrograms = self.decompress_spectrograms(self.compressed_spectrograms)
        self.decompressed = AudioSignal(self.converter.invert_batch_spectrograms(self.decompressed_spectrograms),
                                        self.sample_rate).normalize()

        audio_metrics = self.audio_metrics.get_metrics(self.batch_sig, self.decompressed)
        image_metrics = self.image_metrics.get_metrics(self.spectrograms, self.decompressed_spectrograms)
        bitrate = self.get_bitrate(self.compressed_spectrograms)

        return audio_metrics | image_metrics | bitrate

    def get_summary(self):
        return self.audio_metrics.summary() | self.image_metrics.summary()

    def get_artifacts(self):
        return {"original": self.batch_sig.audio_data[0,:].numpy().squeeze(),
                "decompressed": self.decompressed[0,:].numpy().squeeze(),
                "original_spectrogram": self.spectrograms[0].cpu().numpy(),
                "decompressed_spectrogram": self.decompressed_spectrograms[0].cpu().numpy()}


class CompressionModelILLM(CompressionModelSpectrogram):
    def __init__(self, sample_rate, audio_metrics, image_metrics, converter, quality):
        super().__init__(sample_rate, audio_metrics, image_metrics, converter)
        model = torch.hub.load("facebookresearch/NeuralCompression", quality)
        model = model.to(device)
        model = model.eval()
        model.update()
        model.update_tensor_devices("compress")
        self.model = model

    def compress_spectrograms(self, spectrograms):
        spectrograms = spectrograms.to(device)
        with torch.no_grad():
            return self.model.compress(spectrograms, force_cpu=False)

    def decompress_spectrograms(self, compressed_data):
        with torch.no_grad():
            return self.model.decompress(compressed_data, force_cpu=False).clamp(0.0, 1.0)


class CompressionModelILLMFinetuned(CompressionModelSpectrogram):
    def __init__(self, sample_rate, audio_metrics, image_metrics, converter, checkpoint_path, cfg_path):
        super().__init__(sample_rate, audio_metrics, image_metrics, converter)
        # Load model from checkpoint
        self.checkpoint_path = checkpoint_path
        self.cfg_path = cfg_path
        with open(cfg_path, "rb") as cfg_pkl:
            self.model_cfg = pickle.load(cfg_pkl)
        self.module = build_module(self.model_cfg)
        state_dict = pretrained_state_dict(checkpoint_path)
        model = self.module.model
        model.load_state_dict(state_dict)
        model.to(device)
        model.eval()
        model.update()
        model.update_tensor_devices("compress")
        self.model = model

    def compress_spectrograms(self, spectrograms):
        spectrograms = spectrograms.to(device)
        with torch.no_grad():
            return self.model.compress(spectrograms, force_cpu=False)

    def decompress_spectrograms(self, compressed_data):
        with torch.no_grad():
            return self.model.decompress(compressed_data, force_cpu=False).clamp(0.0, 1.0)
