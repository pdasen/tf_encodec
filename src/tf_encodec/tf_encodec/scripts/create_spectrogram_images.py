# Create a set of spectrogram images from audio dataset
import os
import numpy as np
from tifresi.stft import GaussTruncTF
from tifresi.transforms import log_spectrogram
import torch
from audiotools.data.datasets import AudioDataset, AudioLoader
from audiotools import transforms as tfm
from PIL import Image
import argbind


# Parameters
n_stft = 512
hop_size = 128
db_range = 50
sample_rate = 44100 # Audio gets resampled to this

def create_output_dir(path):
    if not os.path.exists(path):
        os.mkdir(path)

# How many seconds of audio to use such that the spectrogram is square
def get_duration(sample_rate=sample_rate, n_stft=n_stft, hop_size=hop_size):
    #samples = n_stft * hop_size + (n_stft - hop_size)
    samples = n_stft * hop_size/2
    return samples / sample_rate

# Convert scales
def log_spec_to_img(log_S):
    img = (log_S + 50) * 256 / 50
    img = np.repeat(img[..., np.newaxis], 3, 2)
    return np.squeeze(img.astype(np.uint8))

def img_to_log_spec(img):
    # Take average over RGB channels
    #img = torch.mean(img, axis=0)
    log_S = img * 50 - 50
    return log_S

# Create audio dataloader with audiotools
def get_dataloader(path, num_images=20):
    loader = AudioLoader(
        sources=[path],
        transform=None,
        ext=["wav", "mp3", "flac"],
        shuffle_state=0,
    )
    dataset = AudioDataset(
        loaders = [loader],
        sample_rate = 44100,
        num_channels = 1,
        n_examples = num_images,
        duration = get_duration(),
        transform = tfm.RescaleAudio(),
    )
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=1,
        num_workers=0,
        collate_fn=dataset.collate,
    )
    return dataloader

# Create spectrogram from audio
def create_spectrogram(stft_system, sig, n_stft=n_stft):
    Y = stft_system.spectrogram(sig)
    log_Y= log_spectrogram(Y)
    # Truncate to 256 height
    log_Y = log_Y[:n_stft//2, :]
    return log_Y

@argbind.bind()
def main(
    audio_data_path: str = "/home/philipp/projects/tf_encodec/assets/audio/",
    output_path: str = "/home/philipp/projects/tf_encodec/assets/spectrograms/",
    num_images: int = 20
):
    # Create STFT system
    stft_system = GaussTruncTF(hop_size=hop_size, stft_channels=n_stft)

    # Create dataloader
    dataloader = get_dataloader(audio_data_path, num_images)
    create_output_dir(output_path)

    # Create spectrogram images
    for i, datum in enumerate(dataloader):
        if i >= num_images:
            break
        sig = np.squeeze(datum["signal"].audio_data.numpy())
        log_Y = create_spectrogram(stft_system, sig)
        img = log_spec_to_img(log_Y)
        img = Image.fromarray(img, "RGB")
        # Save numpy array to disk
        # np.save(output_path + str(i) + ".npy", img)
        # Save image to disk
        img.save(output_path + str(i) + ".png")

if __name__ == "__main__":
    args = argbind.parse_args()
    with argbind.scope(args):
        main()
