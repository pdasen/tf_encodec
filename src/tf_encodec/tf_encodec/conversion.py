import math
from typing import Optional
import warnings
from audiotools.data.datasets import AudioDataset, AudioLoader

import numpy as np
import torch
import torch.nn as nn
from einops import reduce
from torchaudio.transforms import MelScale
from tifresi import pyplot as plt
from tifresi.hparams import HParams
from tifresi.stft import GaussTF, GaussTruncTF
from tifresi.transforms import log_spectrogram
from tifresi.transforms import inv_log_spectrogram
from omegaconf import DictConfig, OmegaConf
from tf_encodec.utils import get_config
from audiotools import transforms as tfm

class TFConverter:
    def __init__(self,
                 n_stft: int = 512,
                 hop_size: int = 128,
                 db_range: int = 50,
                 sample_rate: int = 44100,):

        self.n_stft = n_stft
        self.hop_size = hop_size
        self.db_range = db_range
        self.sample_rate = sample_rate

        self.stft_system = self.get_stft_system()

    def get_stft_system(self):
        return GaussTF(hop_size=self.hop_size,
                       stft_channels=self.n_stft)

    # How many seconds of audio to use such that the spectrogram is square
    def get_duration_square(self):
        #samples = n_stft * hop_size + (n_stft - hop_size)
        samples = self.n_stft * self.hop_size/2
        return samples / self.sample_rate

    def get_spectrogram_length(self, sig_length):
        trunc_length = sig_length - (sig_length % self.n_stft)
        return math.floor(trunc_length / self.hop_size)

    def get_signal_length(self, spectrogram_length):
        return spectrogram_length * self.hop_size

    # Create spectrogram from audio
    # Expects one channel numpy array
    def get_spectrogram(self, sig, truncate=True):
        # Truncate to modulo stft length
        if truncate:
            sig = sig[:len(sig) - (len(sig) % self.n_stft)]
            #warnings.warn("Truncating signal for spectrogram computation")
        Y = self.stft_system.spectrogram(sig)
        log_Y = log_spectrogram(Y)
        # Truncate height
        log_Y = log_Y[:self.n_stft//2, :]
        return log_Y

    # input: B x 1 x L
    def convert_batch_to_spectrograms(self, batch):
        out = torch.zeros((batch.shape[0], 3, self.n_stft//2, self.get_spectrogram_length(batch.shape[2])))
        for i, y in enumerate(batch):
            log_Y = torch.Tensor(self.get_spectrogram(np.array(torch.squeeze(y))))
            out[i,:] = log_spec_to_img(log_Y)
        return out

    def invert_spectrogram(self, log_Y):
        # TODO make sure this is correct
        # TODO handle the 50db cutoff like discussed
        # Duplicate top row of spectrogram
        log_Y = np.concatenate((log_Y, log_Y[-1:, :]), axis=0)
        Y = inv_log_spectrogram(log_Y)
        return self.stft_system.invert_spectrogram(Y)

    def invert_batch_spectrograms(self, batch_Y):
        # TODO duplicate dim calculation
        reconstructed_y = torch.zeros((batch_Y.shape[0], 1, self.get_signal_length(batch_Y.shape[3])))
        for i, Y in enumerate(batch_Y):
            log_Y = img_to_log_spec(Y)
            reconstructed_y[i] = torch.Tensor(self.invert_spectrogram(np.array(log_Y)))
        return reconstructed_y

    def batch_img_to_mel_spec(self, imgs):
        # Take average over RGB channels
        imgs = reduce(imgs, 'b c h w -> b h w ', 'mean')
        # Back to log scale
        log_S = imgs * 50 - 50
        # Inverse log
        #S = np.exp(log_S)
        # Mel scale transform
        mels = MelScale(n_mels=80, sample_rate=self.sample_rate, n_stft=self.n_stft)(log_S)
        return mels

class MelSpecLoss(nn.Module):
    def __init__(self,
                 n_stft: int = 512,
                 sample_rate: int = 44100,
                 n_mels: int = 80,):

        super().__init__()
        self.mel_scale = MelScale(n_mels=n_mels,
                                  sample_rate=sample_rate,
                                  n_stft=n_stft)

    def forward(self, x, y):
        # Take average over RGB channels
        x = reduce(x, 'b c h w -> b h w ', 'mean')
        y = reduce(y, 'b c h w -> b h w ', 'mean')
        # Back to log scale
        #x = x * 50 - 50
        #y = y * 50 - 50
        return torch.mean((self.mel_scale(x) - self.mel_scale(y))**2)


# AudioDataset class that uses Tifresi for spectrogram computation
class TFDataset(AudioDataset):
    def __init__(self,
                 *args,
                 n_stft: int = 512,
                 hop_size: int = 128,
                 db_range: int = 50,
                 sample_rate: int = 44100,
                 square_duration: bool = False,
                 return_images: bool = True,
                 **kwargs):
        super().__init__(*args, sample_rate=sample_rate, **kwargs)
        self.tf_converter = TFConverter(n_stft=n_stft,
                                        hop_size=hop_size,
                                        db_range=db_range,
                                        sample_rate=sample_rate)
        if square_duration:
            self.duration = self.tf_converter.get_duration_square()
        self.return_images = return_images

    def __getitem__(self, idx):
        item = super().__getitem__(idx)
        audio_data = item["signal"].audio_data
        if self.return_images:
            spectrogram = self.tf_converter.get_spectrogram(audio_data.squeeze().numpy())
            return log_spec_to_img(torch.Tensor(spectrogram))
        else:
            # Remove batch dimension
            return audio_data[0]

def test_tfdataset():
    loader = AudioLoader(
        sources=["/home/philipp/projects/tf_encodec/assets/audio/"],
        transform=tfm.Equalizer(),
        ext=["wav", "mp3", "flac"],
        shuffle_state=0,
    )
    dataset = TFDataset(
        loaders = [loader],
        sample_rate = 44100,
        num_channels = 1,
        n_examples = 10,
        duration = 2,
        transform = tfm.RescaleAudio(),
    )
    return dataset

# Convert scales
def log_spec_to_img(log_S):
    img = (log_S + 50) / 50
    return img.reshape(*(1, *img.shape)).repeat_interleave(3, dim=0)

def img_to_log_spec(img):
    # Take average over RGB channels
    img = torch.mean(img, axis=0)
    log_S = img * 50 - 50
    return log_S

def batch_img_to_log_spec(imgs):
    # Take average over RGB channels
    imgs = reduce(imgs, 'b c h w -> b h w ', 'mean')
    return imgs * 50 - 50
