import os
from audiotools import AudioSignal
import torch
import numpy as np
from visqol import visqol_lib_py
from visqol.pb2 import visqol_config_pb2
from visqol.pb2 import similarity_result_pb2

mode = "audio"

config = visqol_config_pb2.VisqolConfig()

if mode == "audio":
    target_sr = 48000
    config.options.use_speech_scoring = False
    svr_model_path = "libsvm_nu_svr_model.txt"
elif mode == "speech":
    target_sr = 16000
    config.options.use_speech_scoring = True
    svr_model_path = "lattice_tcditugenmeetpackhref_ls2_nl60_lr12_bs2048_learn.005_ep2400_train1_7_raw.tflite"
else:
    raise ValueError(f"Unrecognized mode: {mode}")
config.audio.sample_rate = target_sr
config.options.svr_model_path = os.path.join(
    os.path.dirname(visqol_lib_py.__file__), "model", svr_model_path
)

api = visqol_lib_py.VisqolApi()
api.Create(config)

sig1 = AudioSignal.excerpt("../../../assets/audio/judaspriest.flac", duration=9)
sig2 = AudioSignal.excerpt("../../../assets/audio/earth.flac", duration=9)
noise = AudioSignal(np.random.uniform(-1, 1, sig1.audio_data.shape), sig1.sample_rate)

#estimates = sig1.clone().to_mono().resample(target_sr)
estimates = noise.clone().to_mono().resample(target_sr)
references = sig2.clone().to_mono().resample(target_sr)

visqols = []
for i in range(estimates.batch_size):
    _visqol = api.Measure(
        references.audio_data[i, 0].detach().cpu().numpy().astype(float),
        estimates.audio_data[i, 0].detach().cpu().numpy().astype(float),
    )
    visqols.append(_visqol.moslqo)

print(visqols)
