import contextlib
import os
import warnings

from audiotools import AudioSignal
from hydra import initialize, compose

# For interactive use
def get_config(config_name="config", overrides=[]):
    with initialize(version_base=None, config_path="../conf"):
        return compose(config_name=config_name, overrides=overrides)

def judas_priest():
    sig = AudioSignal.excerpt("../../../assets/audio/judaspriest.flac", duration=5)
    return sig.to_mono()

@contextlib.contextmanager
def suppress_noise():
    with open(os.devnull, 'w') as devnull:
        with contextlib.redirect_stdout(devnull):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                yield
