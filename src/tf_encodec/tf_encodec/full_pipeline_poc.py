#!/usr/bin/env python3
from pathlib import Path

import matplotlib.pyplot as plt
import librosa
import numpy as np
import simpleaudio as sa
from PIL import Image
from tifresi import pyplot as plt
from tifresi.hparams import HParams
from tifresi.stft import GaussTF, GaussTruncTF
from tifresi.transforms import log_spectrogram
from tifresi.transforms import inv_log_spectrogram
import tifresi as tr

import torch
from PIL import Image
from torch import Tensor
from torchmetrics.image import (
    FrechetInceptionDistance,
    LearnedPerceptualImagePatchSimilarity,
)
from torchvision.transforms import ToTensor

# Convert scales
def log_spec_to_img(log_S):
    img = (log_S + 50) / 50
    return np.repeat(img[np.newaxis, ...], 3, 0)

def img_to_log_spec(img):
    # Take average over RGB channels
    img = np.mean(img, axis=0)
    log_S = img * 50 - 50
    return log_S

def load_signal(filename):
    y, _ = tr.utils.load_signal(filename)
    y = tr.utils.preprocess_signal(y)
    return y

def create_spectrogram(sig):
    stft_channels = HParams.stft_channels # 1024
    hop_size = HParams.hop_size # 256

    use_truncated_window = True
    if use_truncated_window:
        stft_system = GaussTruncTF(hop_size=hop_size, stft_channels=stft_channels)
    else:
        stft_system = GaussTF(hop_size=hop_size, stft_channels=stft_channels)

    Y = stft_system.spectrogram(sig)
    log_Y= log_spectrogram(Y)

    # Truncate to 512 height
    log_Y = log_Y[:512, :]

    return log_Y, stft_system

def invert_spectrogram(log_Y, stft_system):
    # Add 513th row back (duplicate of 512th row)
    log_Y = np.concatenate((log_Y, log_Y[-1:, :]), axis=0)
    new_Y = inv_log_spectrogram(log_Y)
    new_y = stft_system.invert_spectrogram(new_Y)

    return new_y

def load_model():
    device = torch.device("cpu")
    model = torch.hub.load("facebookresearch/NeuralCompression", "msillm_quality_3")
    model = model.to(device)
    model = model.eval()
    model.update()
    model.update_tensor_devices("compress")
    return model

def compress_uncompress_spectrogram(log_Y):
    model = load_model()

    image = torch.tensor(log_spec_to_img(log_Y), dtype=torch.float32).unsqueeze(0)

    with torch.no_grad():
        compressed = model.compress(image, force_cpu=False)
        decompressed = model.decompress(compressed, force_cpu=False).clamp(0.0, 1.0)

    decompressed = decompressed.squeeze(0).numpy()
    return img_to_log_spec(decompressed)

def run_codec(filename):
    y = load_signal(filename)
    log_Y, stft_system = create_spectrogram(y)
    log_Y = compress_uncompress_spectrogram(log_Y)
    y = invert_spectrogram(log_Y, stft_system)
    return y.astype(np.float32)

def run_comparison(filename):
    y = load_signal(filename)
    log_Y, stft_system = create_spectrogram(y)
    log_Y_uncomp = compress_uncompress_spectrogram(log_Y)
    y = invert_spectrogram(log_Y, stft_system)
    log_Y_rec, _ = create_spectrogram(y)
    compare_spectrograms2(log_Y, log_Y_uncomp, log_Y_rec)

def compare_spectrograms(log_Y, log_Y_hat):
    log_Y = log_Y.astype(np.float32)
    log_Y_hat = log_Y_hat.astype(np.float32)
    plt.subplot(1, 2, 1)
    plt.imshow(log_Y, aspect="auto", origin="lower")
    plt.title("Original")
    plt.subplot(1, 2, 2)
    plt.imshow(log_Y_hat, aspect="auto", origin="lower")
    plt.title("Reconstructed")
    plt.show()

def compare_spectrograms2(log_Y, log_Y_uncomp, log_Y_rec):
    log_Y = log_Y.astype(np.float32)
    log_Y_uncomp = log_Y_uncomp.astype(np.float32)
    log_Y_rec = log_Y_rec.astype(np.float32)
    plt.subplot(1, 3, 1)
    plt.imshow(log_Y, aspect="auto", origin="lower")
    plt.title("Original")
    plt.subplot(1, 3, 2)
    plt.imshow(log_Y_uncomp, aspect="auto", origin="lower")
    plt.title("Uncompressed")
    plt.subplot(1, 3, 3)
    plt.imshow(log_Y_rec, aspect="auto", origin="lower")
    plt.title("Reconstructed")
    plt.show()

def play_audio(y):
    return sa.play_buffer(y, 1, 4, 44100)
