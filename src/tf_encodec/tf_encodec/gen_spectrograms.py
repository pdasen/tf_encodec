from pathlib import Path

import librosa
import numpy as np
from PIL import Image
from tifresi import pyplot as plt
from tifresi.hparams import HParams
from tifresi.stft import GaussTF, GaussTruncTF
from tifresi.transforms import log_spectrogram
from tifresi.transforms import inv_log_spectrogram
from tifresi.metrics import projection_loss

import tifresi as tr

show_plots = True

def log_spec_to_img(log_S):
    img = (log_S + 50) * 255 / 50
    return np.repeat(img[..., np.newaxis], 3, -1)

def img_to_log_spec(img):
    log_S = img[..., 0] * 50 / 255 - 50
    return log_S

def load_signal(filename):
    y, sr = tr.utils.load_signal(filename)
    y = tr.utils.preprocess_signal(y)
    return y

def create_spectrogram(sig):
    stft_channels = HParams.stft_channels # 1024
    hop_size = HParams.hop_size # 256

    use_truncated_window = True
    if use_truncated_window:
        stft_system = GaussTruncTF(hop_size=hop_size, stft_channels=stft_channels)
    else:
        stft_system = GaussTF(hop_size=hop_size, stft_channels=stft_channels)

    Y = stft_system.spectrogram(sig)
    log_Y= log_spectrogram(Y)

    return log_Y, Y, stft_system

def invert_spectrogram(log_Y, Y, stft_system):
    new_Y = inv_log_spectrogram(log_Y)
    new_y = stft_system.invert_spectrogram(new_Y)

    return new_y

# Process Folder
def process_folder(path, output_path):
    path = Path(path)
    for filename in list(path.glob("*.wav")):
        y = load_signal(filename)
        log_Y, _, _ = create_spectrogram(y)
        img = log_spec_to_img(log_Y)
        # Duplicate dimensions to get RGB
        # Save Spectrogram as image in output_path
        img = Image.fromarray(img.astype('uint8'), "RGB")
        img.save(output_path + "/" + filename.stem + ".png", "PNG")
