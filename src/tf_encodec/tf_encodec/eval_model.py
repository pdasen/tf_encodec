# Script to evaluate the metrics of model
from audiotools.core import audio_signal
import numpy as np
import torch
import torchaudio
from audiotools import transforms as tfm
from audiotools.data.datasets import AudioDataset, AudioLoader
from audiotools import AudioSignal, STFTParams
from tifresi import pyplot as plt
from tifresi.hparams import HParams
from tifresi.stft import GaussTF, GaussTruncTF
from tifresi.transforms import log_spectrogram
from tifresi.transforms import inv_log_spectrogram
# Image metrics
from torchmetrics.image import (
    FrechetInceptionDistance,
    PeakSignalNoiseRatio,
)
# Audio metrics
from torchmetrics.audio import (
    ScaleInvariantSignalDistortionRatio,
)
# from audiotools.metrics.quality import visqol
import audiotools.metrics as audiomt
import wandb
from omegaconf import DictConfig, OmegaConf
import hydra

# Parameters
n_stft = 512
hop_size = 128
db_range = 50
sample_rate = 44100 # Audio gets resampled to this

image_compression_method = "nocomp"
# image_compression_method = "illm_finetuned_v1"

batch_size = 1
test_data_path = "/home/philipp/projects/tf_encodec/assets/audio/"
n_workers = 1

device = torch.device("cpu")

# AudioSignal class that uses Tifresi for spectrogram computation
class AudioSignalTifresi(AudioSignal):
    def __init__(self, tr_hop_size=64, tr_stft_channels=512, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tr_hop_size = tr_hop_size
        self.tr_stft_channels = tr_stft_channels
        self._stft_system = GaussTruncTF(hop_size=self._stft_params.hop_length,
                                         stft_channels=self._stft_params.window_length)


    def tifresi_stft(self):
         # Only compute for first channel for now
         y = torch.squeeze(self._audio_data[0, :])
         Y = self._stft_system.spectrogram(y)
         return log_spectrogram(Y)

    # def invert_spectrogram(self, log_Y):
    #     # Add 513th row back (duplicate of 512th row)
    #     log_Y = np.concatenate((log_Y, log_Y[-1:, :]), axis=0)
#     new_Y = inv_log_spectrogram(log_Y)k
    #     new_y = self._stft_system.invert_spectrogram(new_Y)

    #     return new_y


# Audio dataloader that returns spectrograms
# class AudioLoaderSpectrogram(AudioLoader):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)


#     def __getitem__(self, idx):
#         # Load audio
#         audio = self._load_audio(idx)

#         # Get spectrogram
#         spectrogram = audio.tifresi_stft()

#         # Get label
#         label = self._get_label(idx)

#         return spectrogram, label



# Audio dataset that returns spectrograms
class AudioDatasetSpectrogram(AudioDataset):
    pass

# How many seconds of audio to use such that the spectrogram is square
def get_duration(sample_rate=sample_rate, n_stft=n_stft, hop_size=hop_size):
    #samples = n_stft * hop_size + (n_stft - hop_size)
    samples = n_stft * hop_size/2
    return samples / sample_rate

# Convert scales
def log_spec_to_img(log_S):
    img = (log_S + 50) / 50
    return img.reshape(*(1, *img.shape)).repeat_interleave(3, dim=0)

def img_to_log_spec(img):
    # Take average over RGB channels
    img = torch.mean(img, axis=0)
    log_S = img * 50 - 50
    return log_S

# Load model
def load_compression_model(method):
    if method == "nocomp":
        return None
    if method == "illm":
        model = torch.hub.load("facebookresearch/NeuralCompression", "msillm_quality_3")
        model = model.to(device)
        model = model.eval()
        model.update()
        model.update_tensor_devices("compress")
        return model
    # elif method == "illm_finetuned_v1":
    #     model = torch.hub.load(
    #         "facebookresearch/illumination-invariant-llamas",
    #         "resnet50_illm",
    #         pretrained=True,
    #     )
    #     model.load_state_dict(
    #         torch.load(
    #             "/home/kevin/Documents/master-thesis/illm_finetuned_v1.pth",
    #             map_location=torch.device("cpu"),
    #         )
    #    )
    else:
        raise ValueError("Invalid image compression method")


# Create audio dataloader with audiotools
def get_dataloader(path):
    #loaders = [
    #     AudioLoader(
    #         sources=[path],
    #         transform=tfm.Equalizer(),
    #         ext=["wav"],
    #     )
    #     for i in range(n_workers)
    # ]
    loader = AudioLoader(
        sources=[path],
        transform=tfm.Equalizer(),
        ext=["wav", "mp3", "flac"],
        shuffle_state=0,
    )

    dataset = AudioDataset(
        loaders = [loader],
        sample_rate = 44100,
        num_channels = 1,
        n_examples = 60,
        duration = get_duration(),
        transform = tfm.RescaleAudio(),
    )
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=10,
        num_workers=0,
        collate_fn=dataset.collate,
    )
    return dataloader

def compress_uncompress_illm(model, spectrograms):
    spectrograms = spectrograms.to(device)
    with torch.no_grad():
        compressed = model.compress(spectrograms, force_cpu=False)
        decompressed = model.decompress(compressed, force_cpu=False).clamp(0.0, 1.0)
    return decompressed


def compress_uncompress(method, model, spectrograms):
    if method == "nocomp":
        return spectrograms
    elif method == "illm":
        return compress_uncompress_illm(model, spectrograms)
    else:
        raise ValueError("Invalid image compression method")


# Create spectrogram from audio
def create_spectrogram(stft_system, sig, n_stft=n_stft):
    Y = stft_system.spectrogram(sig)
    log_Y= log_spectrogram(Y)
    # Truncate to 256 height
    log_Y = log_Y[:n_stft//2, :]
    return log_Y

def convert_batch_to_spectrograms(stft_system, batch):
    batch_y = batch["signal"].audio_data
    out = torch.zeros((batch_y.shape[0], 3, n_stft//2, n_stft//2))
    for i, y in enumerate(batch_y):
        log_Y = torch.Tensor(create_spectrogram(stft_system, np.array(torch.squeeze(y))))
        out[i,:] = log_spec_to_img(log_Y)
    return out

def invert_spectrogram(log_Y, stft_system):
    # TODO make sure this is correct
    # TODO handle the 50db cutoff like discussed
    # Add 513th row back (duplicate of 512th row)
    log_Y = np.concatenate((log_Y, log_Y[-1:, :]), axis=0)
    Y = inv_log_spectrogram(log_Y)
    return stft_system.invert_spectrogram(Y)

def invert_batch_spectrograms(batch_Y, stft_system):
    # TODO duplicate dim calculation
    reconstructed_y = torch.zeros((batch_Y.shape[0], 1, n_stft * hop_size//2))
    for i, Y in enumerate(batch_Y):
        log_Y = img_to_log_spec(Y)
        reconstructed_y[i] = torch.Tensor(invert_spectrogram(np.array(log_Y), stft_system))
    return reconstructed_y


@hydra.main(version_base=None)
def main():
    # Wandb logger
    wandb.init(project="tf_encodec",
               config={
                   "image_compression_method": image_compression_method,
                   "n_stft": n_stft,
                   "hop_size": hop_size,
                   "batch_size": batch_size,
                },
               group="evaluate_model_v1",
               job_type="eval",)

    # Load model
    model = load_compression_model(image_compression_method)

    # Create dataloader
    dataloader = get_dataloader(test_data_path)

    PSNR = PeakSignalNoiseRatio()
    SISDR = ScaleInvariantSignalDistortionRatio()
    #sisdr_loss = audiomt.distance.SISDRLoss()
    #sisdr_tot = 0

    # Create stft system
    stft_system = GaussTruncTF(hop_size=hop_size, stft_channels=n_stft)

    # Compress and uncompress
    for batch in dataloader:
        spectrograms = convert_batch_to_spectrograms(stft_system, batch)
        decompressed = compress_uncompress(image_compression_method, model, spectrograms)
        reconstructed = invert_batch_spectrograms(decompressed, stft_system)

        # Image quality metrics
        psnr = PSNR(spectrograms, decompressed)
        # Audio quality metrics
        # reconstructed = AudioSignal(reconstructed, sample_rate=sample_rate)
        # visqol = audiomt.quality.visqol(reconstructed, batch["signal"])
        sisdr = SISDR(reconstructed, batch["signal"].audio_data)
        wandb.log({"SI-SDR": sisdr, "PSNR": psnr})

    # Aggregate metrics
    psnr_total = PSNR.compute()
    sisdr_total = SISDR.compute()
    wandb.run.summary["PSNR-Total"] = psnr_total
    wandb.run.summary["SI-SDR-Total"] = sisdr_total
    wandb.finish()

if __name__ == "__main__":
    main()
