#!/bin/bash
#SBATCH --mail-type=END # mail configuration: NONE, BEGIN, END, FAIL, REQUEUE, ALL
#SBATCH --output=/itet-stor/dasenp/net_scratch/cluster/jobs/%j.out # where to store the output (%j is the JOBID), subdirectory "jobs" must exist
#SBATCH --error=/itet-stor/dasenp/net_scratch/cluster/jobs/%j.err # where to store error messages
#SBATCH --mem=50G
#SBATCH --nodes=1
#SBATCH --cpus-per-task=4
#SBATCH --gres=gpu:4
#CommentSBATCH --exclude=tikgpu10,tikgpu[06-09]
#SBATCH --nodelist=tikgpu[01-05] # Specify that it should run on this particular node
#CommentSBATCH --account=tik-internal
#CommentSBATCH --constraint='titan_rtx|tesla_v100|titan_xp|a100_80gb'



ETH_USERNAME=dasenp
PROJECT_NAME=tf-encodec
DIRECTORY=/itet-stor/${ETH_USERNAME}/net_scratch/${PROJECT_NAME}
CONDA_ENVIRONMENT=tfc
#mkdir -p ${DIRECTORY}/jobs

# Exit on errors
set -o errexit

# Set a directory for temporary files unique to the job with automatic removal at job termination
#TMPDIR=$(mktemp -d)
#if [[ ! -d ${TMPDIR} ]]; then
#	echo 'Failed to create temp directory' >&2
#	exit 1
#fi
#trap "exit 1" HUP INT TERM
#trap 'rm -rf "${TMPDIR}"' EXIT
#export TMPDIR

# Change the current directory to the location where you want to store temporary files, exit if changing didn't succeed.
# Adapt this to your personal preference
#cd "${TMPDIR}" || exit 1

# Send some noteworthy information to the output log

echo "Running on node: $(hostname)"
echo "In directory: $(pwd)"
echo "Starting on: $(date)"
echo "SLURM_JOB_ID: ${SLURM_JOB_ID}"


[[ -f /itet-stor/${ETH_USERNAME}/net_scratch/conda/bin/conda ]] && eval "$(/itet-stor/${ETH_USERNAME}/net_scratch/conda/bin/conda shell.bash hook)"
conda activate ${CONDA_ENVIRONMENT}
echo "Conda activated"
#cd ${DIRECTORY}

python train_audio.py \
    experiment_name=finetune \
    data.train_audio_path=/itet-stor/dasenp/net_scratch/data/musdb18hq/train_mix/ \
    data.val_audio_path=/itet-stor/dasenp/net_scratch/data/musdb18hq/test_mix/ \
    data.batch_size=16 \
    tifresi.n_stft=512 \
    tifresi.hop_size=64 \
    trainer.max_steps=60000 \
    trainer.val_check_interval=5000 \
    optimizer=model_adamw_disc_const \
    model=hific_autoencoder \
    model.freeze_encoder=true \
    model.freeze_bottleneck=true \
    distortion_loss=mse_lpips \
    distortion_loss.mse_param=150.0 \
    distortion_loss.lpips_param=1.0 \
    distortion_loss.backbone=alex \
    +discriminator=condunet_ch1025_factor8_context220 \
    +lightning_module.generator_weight=0.008 \
    +latent_projector=vqvae_xcit_p8_ch64_cb1024_h8 \
    +pretrained_autoencoder=nogan_target0.14bpp \
    +pretrained_latent_autoencoder=20221108_vqvae_xcit_p8_ch64_cb1024_h8

# Send more noteworthy information to the output log
echo "Finished at: $(date)"

# End the script with exit code 0
exit 0   
