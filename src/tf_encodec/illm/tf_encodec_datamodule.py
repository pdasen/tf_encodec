import logging
from pathlib import Path
from typing import Callable, List, Optional

#from default_transforms import default_train_transform, default_val_transform
from lightning.pytorch import LightningDataModule
#from PIL.Image import Image
from torch import Tensor
from torch.utils.data import DataLoader

from audiotools.data.datasets import AudioDataset, AudioLoader, ResumableDistributedSampler
from audiotools import transforms as tfm
from tf_encodec.conversion import TFDataset
#from neuralcompression.data import OpenImagesV6


class AudiotoolsDataModule(LightningDataModule):
    def __init__(
        self,
        train_audio_path,
        batch_size: int,
        n_stft: int = 512,
        hop_size: int = 128,
        db_range: int = 50,
        sample_rate: int = 44100,
        val_audio_path = None,
        workers: int = 0,
        sample_duration: Optional[float] = None,
        #image_size: Optional[List[int]] = None,
        #train_transform: Optional[Callable[[Image], Tensor]] = None,
        #val_transform: Optional[Callable[[Image], Tensor]] = None,
        full_size: bool = False,
        use_async: bool = False,
        pin_memory: bool = True,
        val_batch_size: Optional[int] = None,
        return_images: bool = True,
    ):
        super().__init__()
        self.train_audio_path = train_audio_path

        if val_audio_path is None:
            self.val_audio_path = train_audio_path
        else:
            self.val_audio_path = val_audio_path

        self.batch_size = batch_size
        self.val_batch_size = val_batch_size
        self.workers = workers
        self.full_size = full_size
        self.prepare_data_per_node = True
        self.use_async = use_async
        self.pin_memory = pin_memory
        self.logger = logging.getLogger(self.__class__.__name__)
        # if train_transform is None:
        #     if image_size is None:
        #         raise ValueError("Must pass image_size if no train_transform.")
        #     #self.train_transform = default_train_transform(image_size)
        # else:
        #     self.train_transform = train_transform
        # if val_transform is None:
        #     if image_size is None:
        #         raise ValueError("Must pass image_size if no val_transform.")
        #     #self.val_transform = default_val_transform(image_size)
        # else:
        #     self.val_transform = val_transform

        # Audio Params
        self.n_stft = n_stft
        self.hop_size = hop_size
        self.db_range = db_range
        self.sample_rate = sample_rate

        self.sample_duration = sample_duration
        if sample_duration is None:
            self.square_duration = True
        else:
            self.square_duration = False
        self.return_images = return_images


    def setup(self, stage: Optional[str] = None):
        self.logger.info(f"Audio Train Path: {self.train_audio_path}")
        train_loader = AudioLoader(
            sources=[self.train_audio_path],
            transform=tfm.Equalizer(),
            ext=["wav", "mp3", "flac"],
            shuffle_state=0,
        )
        self.train_dataset = TFDataset(
            loaders = [train_loader],
            n_stft = self.n_stft,
            hop_size = self.hop_size,
            db_range = self.db_range,
            sample_rate = self.sample_rate,
            num_channels = 1,
            n_examples = 350000,
            duration = self.sample_duration,
            square_duration = self.square_duration,
            transform = tfm.RescaleAudio(),
            return_images = self.return_images,
        )
        eval_loader = AudioLoader(
            sources=[self.val_audio_path],
            transform=tfm.Equalizer(),
            ext=["wav", "mp3", "flac"],
            shuffle_state=0,
        )
        self.eval_dataset = TFDataset(
            loaders = [eval_loader],
            n_stft = self.n_stft,
            hop_size = self.hop_size,
            db_range = self.db_range,
            sample_rate = self.sample_rate,
            num_channels = 1,
            n_examples = 1000,
            duration = self.sample_duration,
            square_duration = self.square_duration,
            transform = tfm.RescaleAudio(),
            return_images = self.return_images,
        )

    def train_dataloader(self):
        dataloader = DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            num_workers=self.workers,
            pin_memory=self.pin_memory,
        )
        return dataloader

    def val_dataloader(self):
        if self.val_batch_size is not None:
            batch_size = self.val_batch_size
        else:
            batch_size = self.batch_size

        dataloader = DataLoader(
            self.eval_dataset,
            batch_size=self.batch_size,
            num_workers=self.workers,
            pin_memory=self.pin_memory,
        )

        return dataloader

    def test_dataloader(self):
        return self.val_dataloader()
