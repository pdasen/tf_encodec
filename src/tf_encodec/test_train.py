#!/bin/bash
#SBATCH --mail-type=END # mail configuration: NONE, BEGIN, END, FAIL, REQUEUE, ALL
#SBATCH --output=/itet-stor/dasenp/net_scratch/cluster/jobs/%j.out # where to store the output (%j is the JOBID), subdirectory "jobs" must exist
#SBATCH --error=/itet-stor/dasenp/net_scratch/cluster/jobs/%j.err # where to store error messages
#SBATCH --mem=50G
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#CommentSBATCH --gres=gpu:2
#CommentSBATCH --exclude=tikgpu10,tikgpu[06-09]
#SBATCH --nodelist=arton01 # Specify that it should run on this particular node
#CommentSBATCH --account=tik-internal
#CommentSBATCH --constraint='titan_rtx|tesla_v100|titan_xp|a100_80gb'



ETH_USERNAME=dasenp
PROJECT_NAME=tf-encodec
DIRECTORY=/itet-stor/${ETH_USERNAME}/net_scratch/${PROJECT_NAME}
CONDA_ENVIRONMENT=tfc
#mkdir -p ${DIRECTORY}/jobs

# Exit on errors
set -o errexit

# Set a directory for temporary files unique to the job with automatic removal at job termination
#TMPDIR=$(mktemp -d)
#if [[ ! -d ${TMPDIR} ]]; then
#	echo 'Failed to create temp directory' >&2
#	exit 1
#fi
#trap "exit 1" HUP INT TERM
#trap 'rm -rf "${TMPDIR}"' EXIT
#export TMPDIR

# Change the current directory to the location where you want to store temporary files, exit if changing didn't succeed.
# Adapt this to your personal preference
#cd "${TMPDIR}" || exit 1

# Send some noteworthy information to the output log

echo "Running on node: $(hostname)"
echo "In directory: $(pwd)"
echo "Starting on: $(date)"
echo "SLURM_JOB_ID: ${SLURM_JOB_ID}"


[[ -f /itet-stor/${ETH_USERNAME}/net_scratch/conda/bin/conda ]] && eval "$(/itet-stor/${ETH_USERNAME}/net_scratch/conda/bin/conda shell.bash hook)"
conda activate ${CONDA_ENVIRONMENT}
echo "Conda activated"
#cd ${DIRECTORY}

python scripts/train.py --args.load conf_tfdac/ablations/baseline2.yml --save_path runs/baseline/

# Send more noteworthy information to the output log
echo "Finished at: $(date)"

# End the script with exit code 0
exit 0
