#!/usr/bin/env python3
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import wandb
from audiotools.data.datasets import AudioDataset, AudioLoader
from audiotools import transforms as tfm
from omegaconf import DictConfig
import hydra
import torch
from tf_encodec.evaluation import AudioMetrics, ImageMetrics
from hydra import initialize,compose

sv_fold = "/home/philipp/ownCloud/Thesis/Figures/"

plt.rcParams['text.latex.preamble']=r"\usepackage{lmodern}"
params = {'text.usetex' : True,
                    'font.size' : 13,
                    'font.family' : 'lmodern'}
plt.rcParams.update(params)


def get_duration(cfg):
    # Choose a duration that is a multiple of square spectrogram length equivalent
    square_duration = cfg["tifresi"]["n_stft"] * cfg["tifresi"]["hop_size"] / (2 * cfg["audio"]["sample_rate"])
    actual_duration = cfg.duration - (cfg.duration % -square_duration)
    return actual_duration

def get_dataset(cfg: DictConfig):
    loader = AudioLoader(
        sources=["/home/philipp/projects/tf_encodec/assets/audio/"],
        transform=tfm.Equalizer(),
        ext=["wav", "mp3", "flac"],
        shuffle_state=0,
    )
    dataset = AudioDataset(
        loaders = [loader],
        sample_rate = 44100,
        num_channels = 1,
        n_examples = 100,
        duration = get_duration(cfg),
        transform = tfm.RescaleAudio(),
    )
    return dataset



def get_compressor(cfg):
    converter = hydra.utils.instantiate(cfg.tifresi, sample_rate=cfg.audio.sample_rate)
    audio_metrics = AudioMetrics(enable_fad=False)
    if cfg.evaluate._target_ in [ "tf_encodec.evaluation.CompressionModelSpectrogram",
                                  "tf_encodec.evaluation.CompressionModelILLM", ]:
        image_metrics = ImageMetrics()
        compressor = hydra.utils.instantiate(cfg.evaluate,
                                             cfg.audio.sample_rate,
                                             audio_metrics,
                                             image_metrics,
                                             converter)
    else:
        compressor = hydra.utils.instantiate(cfg.evaluate,
                                             cfg.audio.sample_rate,
                                             audio_metrics)

    return converter, compressor


def get_config(config_name="evaluate_model_illm", overrides=[]):
    with initialize(version_base=None, config_path="conf"):
        return compose(config_name=config_name, overrides=overrides)


def get_wandb_data(run_id):
    api = wandb.Api()
    run = api.run(run_id)
    return run.scan_history()

def save_fig(fig, name):
    fig.savefig(sv_fold + name + ".png", bbox_inches='tight',pad_inches = 0.01, dpi = 200)
    plt.close(fig)

#fig.savefig('doppler_matrix_multi.png', bbox_inches='tight',pad_inches = 0, dpi = 200)

def cons_plot(save=False):
    run_id = "tf-encodec/illm/9b6c8d99f9184b123499619455b21c081eea34f22f96baaffb48fd5598da978f"
    history = get_wandb_data(run_id)

    consistencies = [row["train/consistency"] for row in history]
    ms_ssim = [row["train/ms_ssim"] for row in history]
    global_step = [row["trainer/global_step"] for row in history]
    mask = [ item != None for item in consistencies ]
    y1 = np.array(consistencies)[mask]
    y2 = np.array(ms_ssim)[mask]
    x = np.array(global_step)[mask]

    # Make plot
    fig, ax = plt.subplots()
    ax.plot(x, y1, label='Consistency')
    #ax.plot(x, y2, label='MS-SSIM')
    ax.set(xlabel='Training Step')
    ax.grid()
    if save:
        save_fig(fig, "consistency2")
    else: plt.show()

def msssim_psnr_plot(save=False):
    run_id = "tf-encodec/illm/9b6c8d99f9184b123499619455b21c081eea34f22f96baaffb48fd5598da978f"
    history = get_wandb_data(run_id)

    psnr = [row["train/psnr"] for row in history]
    ms_ssim = [row["train/ms_ssim"] for row in history]
    global_step = [row["trainer/global_step"] for row in history]
    mask = [ item != None for item in psnr ]
    y1 = np.array(ms_ssim)[mask]
    y2 = np.array(psnr)[mask]
    x = np.array(global_step)[mask]

    # Make plot
    fig, ax1 = plt.subplots()
    ax1.plot(x, y1, label='MS-SSIM', alpha=0.85)
    ax1.set_ylabel('MS-SSIM')
    ax1.set(xlabel='Training Step')
    ax1.grid()

    ax2 = ax1.twinx()
    ax2.set_ylabel('PSNR')
    ax2.plot(x, y2, alpha=0.4, color="#ff7f0e")
    if save:
        save_fig(fig, "msssim_psnr")
    else: plt.show()



def get_mos_plot(save=False):
    # Load csv
    mos = np.loadtxt("mos_data.csv", delimiter=",")
    fig,ax = plt.subplots(figsize =(9, 6))

    # Creating axes instance
    #ax = fig.add_axes([0, 0, 1, 1])

    # Creating plot
    #bp = ax.boxplot(mos)
    ax.set_ylabel("Mean Opinion Score (MOS)")

    mins = mos.min(0)
    maxes = mos.max(0)
    means = mos.mean(0)
    std = mos.std(0)

    # create stacked errorbars:
    ax.set_xticks(np.arange(5))
    ax.errorbar(np.arange(5), means, std, fmt='ok', lw=3)
    ax.set_xticklabels(["PGHI only", "ILLM finetuned", "ILLM stock", "Encodec", "Descript"])


    if save:
        save_fig(fig, "MOS_plot")
    else: plt.show()


def save_all():
    cons_plot(save=True)
    msssim_psnr_plot(save=True)
    get_mos_plot(save=True)

if __name__ == "__main__":
    save_all()


def spectrogram_example():
    cfg1 = get_config(overrides=[
        "evaluate=illm_stock",
        "tifresi=stft512",
        "tifresi.hop_size=64"
    ])

    dataset = get_dataset(cfg1)
    #dataset2 = get_dataloader(cfg2)

    tf1, compr1 = get_compressor(cfg1)

    idx = 1
    batch = dataset.__getitem__(idx)

    _ = compr1.evaluate_batch(batch)

    gt = batch["signal"]
    var1 = compr1.decompressed
