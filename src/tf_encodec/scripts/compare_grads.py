import os

import hydra
import torch


def compare_files(path1, path2):
    d1 = torch.load(path1)
    d2 = torch.load(path2)

    equal_list = [torch.equal(v, d2[k]) for k, v in d1.items()]

    if all(equal_list):
        print("All equal")
    else:
        print("Not all equal")


def compare_folders(path1, path2):
    # Iterate over all files ending with .pt contained in both paths
    files1 = [
        f
        for f in os.listdir(path1)
        if os.path.isfile(os.path.join(path1, f)) and f.endswith(".pt")
    ]
    files2 = [
        f
        for f in os.listdir(path2)
        if os.path.isfile(os.path.join(path2, f)) and f.endswith(".pt")
    ]

    files1.sort()
    files2.sort()

    for f1, f2 in zip(files1, files2):
        compare_files(os.path.join(path1, f1), os.path.join(path2, f2))


@hydra.main(version_base=None, config_path="../conf", config_name="compare_grads")
def main(cfg):
    path1 = cfg.path1
    path2 = cfg.path2

    if os.path.isfile(path1) and os.path.isfile(path2):
        compare_files(path1, path2)
    elif os.path.isdir(path1) and os.path.isdir(path2):
        compare_folders(path1, path2)
    else:
        raise ValueError("Either both paths must be files or both paths must be folders")


if __name__ == "__main__":
    main()
