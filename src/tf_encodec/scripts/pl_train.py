import os
import sys
import warnings
from dataclasses import dataclass
from pathlib import Path

import argbind
from lightning.pytorch.plugins import TorchSyncBatchNorm
from tfdac.model import discriminator
import torch
from audiotools import AudioSignal
from audiotools import ml
from audiotools.core import util
from audiotools.data import transforms
from audiotools.data.datasets import AudioDataset
from audiotools.data.datasets import AudioLoader
from audiotools.data.datasets import ConcatDataset
from audiotools.ml.decorators import timer
from audiotools.ml.decorators import Tracker
from audiotools.ml.decorators import when
from torch.utils.tensorboard import SummaryWriter

import lightning.pytorch as pl
import torch
import torch.nn as nn
import yaml
from lightning.pytorch.callbacks import LearningRateMonitor, ModelCheckpoint
from lightning.pytorch.strategies import DDPStrategy

import torch.nn.functional as F

import tfdac

# Todo-list
# DONE Get script to run with PyTorch Lightning trainer
# DONE Pass correct arguments to trainer
# DONE Implement sample saving
# DONE Remove the accel stuff somehow
# DONE Switch to using PyTorch Lightning optimizers
# TODO Test with cluster setup and multiple GPUs
# TODO Add validation dataloader to trainer
# TODO Add checkpointing
# TODO Lightning use correct log directory
#
# TODO Add WandB logging
# TODO Switch to using tf datamodule
# TODO Clean up imports
# TODO Switch to Hydra config?

# DescriptAI training script converted to pytorch lightning

warnings.filterwarnings("ignore", category=UserWarning)

# Enable cudnn autotuner to speed up training
# (can be altered by the funcs.seed function)
torch.backends.cudnn.benchmark = bool(int(os.getenv("CUDNN_BENCHMARK", 1)))
# Uncomment to trade memory for speed.

# Optimizers
AdamW = argbind.bind(torch.optim.AdamW, "generator", "discriminator")
Accelerator = argbind.bind(ml.Accelerator, without_prefix=True)


@argbind.bind("generator", "discriminator")
def ExponentialLR(optimizer, gamma: float = 1.0):
    return torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma)


# Models
DAC = argbind.bind(tfdac.model.DAC)
Discriminator = argbind.bind(tfdac.model.Discriminator)

# Data
AudioDataset = argbind.bind(AudioDataset, "train", "val")
AudioLoader = argbind.bind(AudioLoader, "train", "val")

# Transforms
filter_fn = lambda fn: hasattr(fn, "transform") and fn.__qualname__ not in [
    "BaseTransform",
    "Compose",
    "Choose",
]
tfm = argbind.bind_module(transforms, "train", "val", filter_fn=filter_fn)

# Loss
filter_fn = lambda fn: hasattr(fn, "forward") and "Loss" in fn.__name__
losses = argbind.bind_module(tfdac.nn.loss, filter_fn=filter_fn)


def get_infinite_loader(dataloader):
    while True:
        for batch in dataloader:
            yield batch


@argbind.bind("train", "val")
def build_transform(
    augment_prob: float = 1.0,
    preprocess: list = ["Identity"],
    augment: list = ["Identity"],
    postprocess: list = ["Identity"],
):
    to_tfm = lambda l: [getattr(tfm, x)() for x in l]
    preprocess = transforms.Compose(*to_tfm(preprocess), name="preprocess")
    augment = transforms.Compose(*to_tfm(augment), name="augment", prob=augment_prob)
    postprocess = transforms.Compose(*to_tfm(postprocess), name="postprocess")
    transform = transforms.Compose(preprocess, augment, postprocess)
    return transform


@argbind.bind("train", "val", "test")
def build_dataset(
    sample_rate: int,
    folders: dict = None,
):
    # Give one loader per key/value of dictionary, where
    # value is a list of folders. Create a dataset for each one.
    # Concatenate the datasets with ConcatDataset, which
    # cycles through them.
    datasets = []
    for _, v in folders.items():
        loader = AudioLoader(sources=v)
        transform = build_transform()
        dataset = AudioDataset(loader, sample_rate, transform=transform)
        datasets.append(dataset)

    dataset = ConcatDataset(datasets)
    dataset.transform = transform
    return dataset

class GANLoss2(nn.Module):
    """
    Computes a discriminator loss, given a discriminator on
    generated waveforms/spectrograms compared to ground truth
    waveforms/spectrograms. Computes the loss for both the
    discriminator and the generator in separate functions.
    """

    def __init__(self):
        super().__init__()

    def forward(self, discriminator, fake, real):
        d_fake = discriminator.forward(fake.audio_data)
        d_real = discriminator.forward(real.audio_data)
        return d_fake, d_real

    def discriminator_loss(self, discriminator, fake, real):
        d_fake, d_real = self.forward(discriminator, fake.clone().detach(), real)

        loss_d = 0
        for x_fake, x_real in zip(d_fake, d_real):
            loss_d += torch.mean(x_fake[-1] ** 2)
            loss_d += torch.mean((1 - x_real[-1]) ** 2)
        return loss_d

    def generator_loss(self, discriminator, fake, real):
        d_fake, d_real = self.forward(discriminator, fake, real)

        loss_g = 0
        for x_fake in d_fake:
            loss_g += torch.mean((1 - x_fake[-1]) ** 2)

        loss_feature = 0

        for i in range(len(d_fake)):
            for j in range(len(d_fake[i]) - 1):
                loss_feature += F.l1_loss(d_fake[i][j], d_real[i][j].detach())
        return loss_g, loss_feature

@dataclass
class State:
    generator: DAC
    optimizer_g: AdamW
    scheduler_g: ExponentialLR

    discriminator: Discriminator
    optimizer_d: AdamW
    scheduler_d: ExponentialLR

    stft_loss: losses.MultiScaleSTFTLoss
    mel_loss: losses.MelSpectrogramLoss
    gan_loss: losses.GANLoss
    waveform_loss: losses.L1Loss

    train_data: AudioDataset
    val_data: AudioDataset

    tracker: Tracker


class TFDACModule(pl.LightningModule):
    def __init__(
            self,
            args,
            accel: ml.Accelerator,
            save_path: str,
            resume: bool = False,
            tag: str = "latest",
            load_weights: bool = False,
            lambdas: dict = {
                "mel/loss": 100.0,
                "adv/feat_loss": 2.0,
                "adv/gen_loss": 1.0,
                "vq/commitment_loss": 0.25,
                "vq/codebook_loss": 1.0,
            },
        ):
        super().__init__()
        generator, g_extra = None, {}
        discriminator, d_extra = None, {}

        if resume:
            kwargs = {
                "folder": f"{save_path}/{tag}",
                "map_location": "cpu",
                "package": not load_weights,
            }
            #tracker.print(f"Resuming from {str(Path('.').absolute())}/{kwargs['folder']}")
            if (Path(kwargs["folder"]) / "dac").exists():
                generator, g_extra = DAC.load_from_folder(**kwargs)
            if (Path(kwargs["folder"]) / "discriminator").exists():
                discriminator, d_extra = Discriminator.load_from_folder(**kwargs)

        generator = DAC() if generator is None else generator
        discriminator = Discriminator() if discriminator is None else discriminator

        # TODO add parameter print
        #tracker.print(generator)
        #tracker.print(discriminator)

        #generator = accel.prepare_model(generator)
        #discriminator = accel.prepare_model(discriminator)

        generator = torch.nn.SyncBatchNorm.convert_sync_batchnorm(generator)
        discriminator = torch.nn.SyncBatchNorm.convert_sync_batchnorm(discriminator)

        with argbind.scope(args, "generator"):
            optimizer_g = AdamW(generator.parameters())
            scheduler_g = ExponentialLR(optimizer_g)
        with argbind.scope(args, "discriminator"):
            optimizer_d = AdamW(discriminator.parameters())
            scheduler_d = ExponentialLR(optimizer_d)

        if "optimizer.pth" in g_extra:
            optimizer_g.load_state_dict(g_extra["optimizer.pth"])
        if "scheduler.pth" in g_extra:
            scheduler_g.load_state_dict(g_extra["scheduler.pth"])
        #if "tracker.pth" in g_extra:
        #    tracker.load_state_dict(g_extra["tracker.pth"])

        if "optimizer.pth" in d_extra:
            optimizer_d.load_state_dict(d_extra["optimizer.pth"])
        if "scheduler.pth" in d_extra:
            scheduler_d.load_state_dict(d_extra["scheduler.pth"])

        sample_rate = generator.sample_rate
        with argbind.scope(args, "train"):
            train_data = build_dataset(sample_rate)
        with argbind.scope(args, "val"):
            val_data = build_dataset(sample_rate)

        waveform_loss = losses.L1Loss()
        stft_loss = losses.MultiScaleSTFTLoss()
        mel_loss = losses.MelSpectrogramLoss()
        #gan_loss = losses.GANLoss(discriminator)
        gan_loss = GANLoss2()

        #self.accel = accel
        self.args = args
        self.automatic_optimization=False
        self.lambdas = lambdas
        self.save_path = save_path

        self.generator=generator
        #self.optimizer_g=optimizer_g
        #self.scheduler_g=scheduler_g
        self.discriminator=discriminator
        #self.optimizer_d=optimizer_d
        #self.scheduler_d=scheduler_d
        self.waveform_loss=waveform_loss
        self.stft_loss=stft_loss
        self.mel_loss=mel_loss
        self.gan_loss=gan_loss
        #self.tracker=tracker
        self.train_data=train_data
        self.val_data=val_data


    @timer()
    def training_step(self, batch, batch_idx):
        optimizer_g, optimizer_d = self.optimizers()
        scheduler_g, scheduler_d = self.lr_schedulers()
        #self.generator.train()
        #self.discriminator.train()
        output = {}

        #batch = util.prepare_batch(batch, accel.device)
        with torch.no_grad():
            signal = self.train_data.transform(
                batch["signal"].clone(), **batch["transform_args"]
            )

        out = self.generator(signal.audio_data, signal.sample_rate)
        recons = AudioSignal(out["audio"], signal.sample_rate)
        commitment_loss = out["vq/commitment_loss"]
        codebook_loss = out["vq/codebook_loss"]

        #output["adv/disc_loss"] = self.gan_loss.discriminator_loss(recons, signal)
        output["adv/disc_loss"] = self.gan_loss.discriminator_loss(self.discriminator, recons, signal)

        optimizer_d.zero_grad()
        #output["adv/disc_loss"].backward()
        self.manual_backward(output["adv/disc_loss"])
        output["other/grad_norm_d"] = torch.nn.utils.clip_grad_norm_(
            self.discriminator.parameters(), 10.0
        )
        optimizer_d.step()
        scheduler_d.step()

        output["stft/loss"] = self.stft_loss(recons, signal)
        output["mel/loss"] = self.mel_loss(recons, signal)
        output["waveform/loss"] = self.waveform_loss(recons, signal)
        # (
        #     output["adv/gen_loss"],
        #     output["adv/feat_loss"],
        # ) = self.gan_loss.generator_loss(recons, signal)
        (
            output["adv/gen_loss"],
            output["adv/feat_loss"],
        ) = self.gan_loss.generator_loss(self.discriminator, recons, signal)
        output["vq/commitment_loss"] = commitment_loss
        output["vq/codebook_loss"] = codebook_loss
        output["loss"] = sum([v * output[k] for k, v in self.lambdas.items() if k in output])

        optimizer_g.zero_grad()
        #output["loss"].backward()
        self.manual_backward(output["loss"])
        output["other/grad_norm"] = torch.nn.utils.clip_grad_norm_(
            self.generator.parameters(), 1e3
        )
        optimizer_g.step()
        scheduler_g.step()

        output["other/learning_rate"] = optimizer_g.param_groups[0]["lr"]
        # TODO Make sure this one is fine
        output["other/batch_size"] = signal.batch_size

        #if batch_idx < 4:
        if True:
                grad_dict_generator = {x[0]:x[1].grad for x in self.generator.named_parameters()}
                grad_dict_discriminator = {x[0]:x[1].grad for x in self.discriminator.named_parameters()}
                torch.save(grad_dict_generator, f"{self.save_path}/grad_dict_generator_{batch_idx}.pt")
                torch.save(grad_dict_discriminator, f"{self.save_path}grad_dict_discriminator_{batch_idx}.pt")

        return {k: v for k, v in sorted(output.items())}


    @timer()
    @torch.no_grad()
    def validation_step(self, batch, batch_idx):
        self.generator.eval()
        #batch = util.prepare_batch(batch, accel.device)
        signal = self.val_data.transform(
            batch["signal"].clone(), **batch["transform_args"]
        )

        out = self.generator(signal.audio_data, signal.sample_rate)
        recons = AudioSignal(out["audio"], signal.sample_rate)

        return {
            "loss": self.mel_loss(recons, signal),
            "mel/loss": self.mel_loss(recons, signal),
            "stft/loss": self.stft_loss(recons, signal),
            "waveform/loss": self.waveform_loss(recons, signal),
        }

    # TODO untested
    @torch.no_grad()
    def get_log_samples(self, val_idx, batch_idx):
        # TODO
        #state.tracker.print("Saving audio samples to TensorBoard")
        self.generator.eval()

        samples = [self.val_data[idx] for idx in val_idx]
        batch = self.val_data.collate(samples)
        #batch = util.prepare_batch(batch, accel.device)
        signal = self.train_data.transform(
            batch["signal"].clone(), **batch["transform_args"]
        )

        out = self.generator(signal.audio_data, signal.sample_rate)
        recons = AudioSignal(out["audio"], signal.sample_rate)

        audio_dict = {"recons": recons}
        if batch_idx == 0:
            audio_dict["signal"] = signal

        return audio_dict


    def configure_optimizers(self):
        with argbind.scope(self.args, "generator"):
            optimizer_g = AdamW(self.generator.parameters())
            scheduler_g = ExponentialLR(optimizer_g)
        with argbind.scope(self.args, "discriminator"):
            optimizer_d = AdamW(self.discriminator.parameters())
            scheduler_d = ExponentialLR(optimizer_d)
        return (
            {
                "optimizer": optimizer_g,
                "lr_scheduler": {
                    "scheduler": scheduler_g,
                },
            },
            {
                "optimizer": optimizer_d,
                "lr_scheduler": {
                    "scheduler": scheduler_d,
                },
            },
        )

    #def configure_optimizers(self):
    #    optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams.lr)
    #    return optimizer

@argbind.bind(without_prefix=True)
def train(
    args,
    accel: ml.Accelerator,
    seed: int = 0,
    save_path: str = "ckpt",
    num_iters: int = 250000,
    save_iters: list = [10000, 50000, 100000, 200000],
    sample_freq: int = 10000,
    valid_freq: int = 1000,
    batch_size: int = 12,
    val_batch_size: int = 10,
    num_workers: int = 8,
    val_idx: list = [0, 1, 2, 3, 4, 5, 6, 7],
    lambdas: dict = {
        "mel/loss": 100.0,
        "adv/feat_loss": 2.0,
        "adv/gen_loss": 1.0,
        "vq/commitment_loss": 0.25,
        "vq/codebook_loss": 1.0,
    },
):
    util.seed(seed)
    Path(save_path).mkdir(exist_ok=True, parents=True)
    # writer = (
    #     SummaryWriter(log_dir=f"{save_path}/logs") if accel.local_rank == 0 else None
    # )
    # tracker = Tracker(
    #     writer=writer, log_file=f"{save_path}/log.txt", rank=accel.local_rank
    # )

    module = TFDACModule(args,
                         accel,
                         save_path,
                         resume=True,
                         tag="latest",
                         load_weights=True,
                         lambdas=lambdas)
    #state = load(args, accel, tracker, save_path)
    train_dataloader = accel.prepare_dataloader(
        module.train_data,
        # TODO make sure resume works
        #module.tracker.step * batch_size,
        start_idx=0,
        num_workers=num_workers,
        batch_size=batch_size,
        collate_fn=module.train_data.collate,
    )
    train_dataloader = get_infinite_loader(train_dataloader)
    val_dataloader = accel.prepare_dataloader(
        module.val_data,
        start_idx=0,
        num_workers=num_workers,
        batch_size=val_batch_size,
        collate_fn=module.val_data.collate,
        persistent_workers=True if num_workers > 0 else False,
    )

    # Wrap the functions so that they neatly track in TensorBoard + progress bars
    # and only run when specific conditions are met.
    # global train_loop, val_loop, validate, save_samples, checkpoint
    # train_loop = tracker.log("train", "value", history=False)(
    #     tracker.track("train", num_iters, completed=state.tracker.step)(train_loop)
    # )
    # val_loop = tracker.track("val", len(val_dataloader))(val_loop)
    # validate = tracker.log("val", "mean")(validate)

    # # These functions run only on the 0-rank process
    # save_samples = when(lambda: accel.local_rank == 0)(save_samples)
    # checkpoint = when(lambda: accel.local_rank == 0)(checkpoint)

    trainer = pl.Trainer(accelerator="auto",
                         strategy="ddp_find_unused_parameters_true",
                         deterministic=False,

                         #max_steps=num_iters,
                         max_steps=6, # just for now
                         # TODO Checkpointing behavior
                         # TODO sample freq callback (using val_idx)
                         val_check_interval=valid_freq
                         plugins=[pl.plugins.TorchSyncBatchNorm()],)
>>>>>>> Stashed changes

    trainer.fit(module, train_dataloader)

    # with tracker.live:
    #     for tracker.step, batch in enumerate(train_dataloader, start=tracker.step):
    #         train_loop(state, batch, accel, lambdas)

    #         last_iter = (
    #             tracker.step == num_iters - 1 if num_iters is not None else False
    #         )
    #         if tracker.step % sample_freq == 0 or last_iter:
    #             save_samples(state, val_idx, writer)

    #         if tracker.step % valid_freq == 0 or last_iter:
    #             validate(state, val_dataloader, accel)
    #             checkpoint(state, save_iters, save_path)
    #             # Reset validation progress bar, print summary since last validation.
    #             tracker.done("val", f"Iteration {tracker.step}")

    #         if last_iter:
    #             break


if __name__ == "__main__":
    args = argbind.parse_args()
    args["args.debug"] = int(os.getenv("LOCAL_RANK", 0)) == 0
    with argbind.scope(args):
        with Accelerator() as accel:
            #if accel.local_rank != 0:
                #sys.tracebacklimit = 0
            train(args, accel)
