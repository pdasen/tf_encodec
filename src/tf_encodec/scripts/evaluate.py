import pickle
import os

import numpy as np
import torch
from audiotools import transforms as tfm
from audiotools.data.datasets import AudioDataset, AudioLoader
from illm.train_audio import build_module, pretrained_state_dict

# Image metrics
from torchmetrics.image import (
    FrechetInceptionDistance,
    PeakSignalNoiseRatio,
)
# Audio metrics
from torchmetrics.audio import (
    ScaleInvariantSignalDistortionRatio,
)
# from audiotools.metrics.quality import visqol
import audiotools.metrics as audiomt
import wandb
from omegaconf import DictConfig, OmegaConf
import hydra
from hydra import initialize, compose

from tf_encodec.conversion import TFConverter
from tf_encodec.evaluation import AudioMetrics, ImageMetrics

device = torch.device("cpu")


# Load model
# TODO Refactor using hydra instantiate
def load_compression_model(cfg: DictConfig):
    method = cfg.evaluate.method
    device = cfg.device
    if method == "bypass":
        return None
    if method == "illm_stock":
        model = torch.hub.load("facebookresearch/NeuralCompression", cfg.evaluate.quality)
        model = model.to(device)
        model = model.eval()
        model.update()
        model.update_tensor_devices("compress")
        return model
    if method == "illm_finetuned":
        # Load model from checkpoint
        checkpoint_path = cfg.evaluate.model_path
        cfg_path = cfg.evaluate.cfg_path
        with open(cfg_path, "rb") as cfg_pkl:
            model_cfg = pickle.load(cfg_pkl)
        module = build_module(model_cfg)
        state_dict = pretrained_state_dict(checkpoint_path)
        model = module.model
        model.load_state_dict(state_dict)

        model = module.model.to(device)
        model = module.model.eval()
        module.model.update()
        model.update_tensor_devices("compress")
        return module.model

    else:
        raise ValueError("Invalid image compression method")



# Create audio dataloader with audiotools
def get_dataloader(cfg: DictConfig):
    loader = AudioLoader(
        sources=[cfg.dataset.test_data, cfg.dataset.train_data],
        transform=tfm.Equalizer(),
        ext=["wav", "mp3", "flac"],
        shuffle_state=0,
    )

    dataset = AudioDataset(
        loaders = [loader],
        sample_rate = 44100,
        num_channels = 1,
        n_examples = cfg.num_evals,
        duration = get_duration(cfg),
        transform = tfm.RescaleAudio(),
    )
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=cfg.batch_size,
        num_workers=cfg.num_workers,
        collate_fn=dataset.collate,
    )
    return dataloader

def get_duration(cfg):
    # Choose a duration that is a multiple of square spectrogram length equivalent
    square_duration = cfg["tifresi"]["n_stft"] * cfg["tifresi"]["hop_size"] / (2 * cfg["audio"]["sample_rate"])
    actual_duration = cfg.duration - (cfg.duration % -square_duration)
    print("Duration:", actual_duration)
    return actual_duration

# For interactive use
def get_config(config_name="evaluate_model_illm", overrides=[]):
    with initialize(version_base=None, config_path="../conf"):
        return compose(config_name=config_name, overrides=overrides)

def transpose_image(image):
    return np.transpose(image, (1, 2, 0))

@hydra.main(version_base=None, config_path="../conf", config_name="evaluate_model_cluster")
def main(cfg: DictConfig):
    # Wandb logger
    with wandb.init(project="tf_encodec",
               # Log config
               config=OmegaConf.to_container(cfg),
               group="evaluate_model_v1",
               job_type="eval",) as run:

        # Define metric summaries
        run.define_metric("psnr", summary="mean")
        run.define_metric("ms-ssim", summary="mean")
        run.define_metric("sisdr", summary="mean")
        run.define_metric("visqol", summary="mean")
        run.define_metric("mel_distance", summary="mean")

        # Create dataloader and metric trackers
        dataloader = get_dataloader(cfg)
        audio_metrics = AudioMetrics()

        if cfg.evaluate._target_ in [ "tf_encodec.evaluation.CompressionModelSpectrogram",
                                      "tf_encodec.evaluation.CompressionModelILLM",
                                      "tf_encodec.evaluation.CompressionModelILLMFinetuned" ]:
            image_metrics = ImageMetrics()
            converter = hydra.utils.instantiate(cfg.tifresi,
                                                sample_rate=cfg.audio.sample_rate)
            compressor = hydra.utils.instantiate(cfg.evaluate,
                                                 cfg.audio.sample_rate,
                                                 audio_metrics,
                                                 image_metrics,
                                                 converter)
        else:
            compressor = hydra.utils.instantiate(cfg.evaluate,
                                                 cfg.audio.sample_rate,
                                                 audio_metrics)

        for batch in dataloader:
            print(run.step, "of", cfg.num_evals)
            metrics = compressor.evaluate_batch(batch)

            if run.step < 20:
                artifacts = compressor.get_artifacts()
                for name, artifact in artifacts.items():
                    if name in ["original", "decompressed"]:
                        run.log({name: wandb.Audio(artifact, sample_rate=cfg.audio.sample_rate)}, commit=False)
                    elif name in ["original_spectrogram", "decompressed_spectrogram"]:
                        run.log({name: wandb.Image(transpose_image(artifact))}, commit=False)
                    else:
                        raise ValueError("Invalid artifact name")


            run.log(metrics)

        # Log FAD separately
        summmary_metrics = compressor.get_summary()
        run.summary["fad"] = summmary_metrics["fad"]



def main2(cfg: DictConfig):
    # Wandb logger
    with wandb.init(project="tf_encodec",
               # Log config
               config=OmegaConf.to_container(cfg),
               group="evaluate_model_v1",
               job_type="eval",) as run:
        print("Wandb run id:", run.id)

        # Load model
        model = load_compression_model(cfg)

        # Create dataloader and metric trackers
        dataloader = get_dataloader(cfg)
        image_metrics = ImageMetrics()
        audio_metrics = AudioMetrics()

        converter = hydra.utils.instantiate(cfg.tifresi,
                                            sample_rate=cfg.audio.sample_rate)

        converter = TFConverter(cfg.tifresi.n_stft,
                                cfg.tifresi.hop_size,
                                cfg.tifresi.db_range,
                                cfg.audio.sample_rate)

        # Compress and uncompress
        for batch in dataloader:
            batch_sig = batch["signal"].normalize()
            batch = batch_sig.audio_data
            spectrograms = converter.convert_batch_to_spectrograms(batch)
            decompressed = compress_uncompress(cfg.evaluate.method, model, spectrograms)
            reconstructed = AudioSignal(converter.invert_batch_spectrograms(decompressed),
                                        sample_rate=cfg.audio.sample_rate).normalize()

            # Log artifacts in first three batches
            if run.step < 3:
                # Convert audio to 1D numpy array float32
                original_log = batch_sig.audio_data[0,:].numpy().squeeze()
                reconstructed_log = reconstructed[0,:].numpy().squeeze()
                # Log audio
                run.log({"audio": wandb.Audio(original_log, sample_rate=cfg["audio"]["sample_rate"])}, commit=False)
                run.log({"reconstructed": wandb.Audio(reconstructed_log, sample_rate=cfg["audio"]["sample_rate"])}, commit=False)
                # Log spectrogram
                run.log({"spectrogram": wandb.Image(transpose_image(spectrograms[0].cpu().numpy()))}, commit=False)
                run.log({"decompressed": wandb.Image(transpose_image(decompressed[0].cpu().numpy()))}, commit=False)

            # Image quality metrics
            i = image_metrics.get_metrics(spectrograms, decompressed)
            # Audio quality metrics
            a = audio_metrics.get_metrics(batch_sig, reconstructed)
            run.log(a | i)

        # Aggregate metrics
        i_tot = image_metrics.summary()
        a_tot = audio_metrics.summary()
        run.summary["psnr"] = i_tot["psnr"]
        run.summary["sisdr"] = a_tot["sisdr"]
        run.summary["visqol"] = a_tot["visqol"]

if __name__ == "__main__":
    main()
