# tf_encodec
TF-Encodec

Installation:

1. Create a conda environment
    ```
    conda create -n tfc python=3.10
    conda activate tfc
    ```
2. Install compilation packages
    ```
    conda install -c conda-forge fftw lapack cmake "cython=0.29.34" bazel=5.32
    ```
3. Install pytorch 2.1.0, for example:
    ```
    conda install pytorch==2.1.0 torchvision==0.16.0 torchaudio==2.1.0 pytorch-cuda=11.8 -c pytorch -c nvidia
    ```
4. Compile and install visqol (might need to adjust linker options)
    ```
    cd [project_folder]/src/visqol
    bazel build :visqol -c opt
    pip install -e .
    ```
5. Install the other dependencies
```
    cd [project_folder]/src/tf_encodec
    pip install -e .
```
