import librosa
import numpy as np
from tifresi import pyplot as plt
from tifresi.hparams import HParams
from tifresi.stft import GaussTF, GaussTruncTF
from tifresi.transforms import log_spectrogram
from tifresi.transforms import inv_log_spectrogram
from tifresi.metrics import projection_loss

import tifresi as tr

show_plots = True

def load_signal():
    filename = librosa.util.example('brahms')
    y, sr = tr.utils.load_signal(filename)
    print(sr)
    y = tr.utils.preprocess_signal(y)
    if show_plots:
        n = 1024*256
        t = np.arange(n)/sr*1000
        plt.plot(t, y[:n])
        plt.xlabel('Time [ms]');
        plt.show();
    return y

def create_spectrogram(sig):
    stft_channels = HParams.stft_channels # 1024
    hop_size = HParams.hop_size # 256

    use_truncated_window = True
    if use_truncated_window:
        stft_system = GaussTruncTF(hop_size=hop_size, stft_channels=stft_channels)
    else:
        stft_system = GaussTF(hop_size=hop_size, stft_channels=stft_channels)

    Y = stft_system.spectrogram(sig)
    log_Y= log_spectrogram(Y)
    if show_plots:
        n = 1024*256
        plt.figure(dpi=200, figsize=(10,3))
        plt.imshow(log_Y[:,:n//hop_size])
        plt.colorbar()
        plt.show()

    return log_Y, Y, stft_system

def invert_spectrogram(log_Y, Y, stft_system):
    new_Y = inv_log_spectrogram(log_Y)
    print(projection_loss(new_Y, Y))

    new_y = stft_system.invert_spectrogram(new_Y)

    new_Yp = stft_system.spectrogram(new_y)
    print(projection_loss(new_Y, new_Yp))
    return new_y
